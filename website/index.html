<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="generator" content="xidoc"><meta name="viewport" content="width=device-width,initial-scale=1"><!--
  © 2023 Adam Blažek <blazead5@cvut.cz>
  SPDX-License-Identifier: GPL-3.0-or-later
--><link rel="icon" href="data:," /><title>Vier</title><link rel="stylesheet" href="xi.min.css" /><style>:root{--fg-accent:#4ea;}
:root{--bg-accent:#275;}
body{font-family:var(--sans-serif);}
dt{font-weight:bold;
font-size:1.0rem;}
table{border-top:1px solid #fff;
border-bottom:1px solid #fff;
border-collapse:collapse;
margin:.4rem 0;}
th{border-bottom:1px solid #fff;}
th,td{padding:.2rem;}code[class*=language-],pre[class*=language-]{font-family:Consolas,Monaco,'Andale Mono','Ubuntu Mono',monospace;font-size:1em;text-align:left;white-space:pre;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}pre[class*=language-]{padding:.4em .8em;margin:.5em 0;overflow:auto;background-color:#000}code[class*=language-]{background:#000;color:#fff;box-shadow:-.3em 0 0 .3em #000,.3em 0 0 .3em #000}:not(pre)>code[class*=language-]{padding:.2em;border-radius:.3em;box-shadow:none;white-space:normal}.token.cdata,.token.comment,.token.doctype,.token.prolog{color:#aaa}.token.punctuation{color:#999}.token.namespace{opacity:.7}.token.boolean,.token.constant,.token.number,.token.property,.token.symbol,.token.tag{color:#0cf}.token.attr-name,.token.builtin,.token.char,.token.selector,.token.string{color:#ff0}.language-css .token.string,.token.entity,.token.inserted,.token.operator,.token.url,.token.variable{color:#9acd32}.token.atrule,.token.attr-value,.token.keyword{color:#ff1493}.token.important,.token.regex{color:orange}.token.bold,.token.important{font-weight:700}.token.italic{font-style:italic}.token.entity{cursor:help}.token.deleted{color:red}pre.diff-highlight.diff-highlight>code .token.deleted:not(.prefix),pre>code.diff-highlight.diff-highlight .token.deleted:not(.prefix){background-color:rgba(255,0,0,.3);display:inline}pre.diff-highlight.diff-highlight>code .token.inserted:not(.prefix),pre>code.diff-highlight.diff-highlight .token.inserted:not(.prefix){background-color:rgba(0,255,128,.3);display:inline}
</style></head><body>
<h1>Vier</h1>
<p><dfn>Vier</dfn> is a keyboard-focused pixel art editor heavily inspired by Vim.</p>
<figure><img src="./screenshot.png" alt="A screenshot of Vier" /></figure>
<section><h2 class="xd-section-heading">Features</h2><ul><li>Can be fully used without a mouse, though drawing with a mouse is also supported</li><li>Modal editing</li><li>Edit multiple images</li><li>Uncluttered, minimalistic UI</li><li>Uncompromised performance: everything happens instantly, including startup</li><li>Works directly with images, no need to bother with project files</li></ul></section>
<section><h2 class="xd-section-heading">Source code</h2><p>The source code is available <a href="https://git.sr.ht/~xigoi/vier/">on SourceHut</a> or <a href="https://gitlab.fjfi.cvut.cz/blazead5/vier/">on my faculty’s GitLab instance</a>.</p></section>
<section><h2 class="xd-section-heading">Installation</h2><p>Vier is written in <a href="https://nim-lang.org/">Nim</a>, so the language and its tooling must be installed.</p>
<p>For GNU/Linux, just use the command</p>
<pre class="language-sh"><code class="language-sh">nimble <span class="token function">install</span> https://git.sr.ht/~xigoi/vier
</code></pre>
<p>Alternatively, you can install directly from the repository:</p>
<pre class="language-sh"><code class="language-sh"><span class="token function">git</span> clone https://git.sr.ht/~xigoi/vier
nimble <span class="token function">install</span>
</code></pre>
<p>Note that you need to have <code>$HOME/.nimble/bin</code> in your <code>$PATH</code>.</p>
<p>Also note that since Nimble does not support optional dependencies, it will download transitive dependencies that are not actually needed for the project. I am looking into ways to solve this issue.</p></section>
<section><h2 class="xd-section-heading">Usage</h2><p>Launch Vier via the command line on the picture(s) you want to edit:</p>
<pre class="language-sh"><code class="language-sh">vier cat.png dog.png
</code></pre>
<p>Alternatively, you can launch it without arguments to create a new picture:</p>
<pre class="language-sh"><code class="language-sh">vier
</code></pre></section>
<section><h2 class="xd-section-heading">Modes and Tools</h2>In most graphics editing programs, there is the concept of a <dfn>tool</dfn>, a state of the editor which determines what action is performed when clicking on an image. This is also the case for Vier, but like in Vim, there is also the concept of <dfn>modes</dfn> – a state which affects more generally how keys are interpreted.
<p>Vier has these modes:</p>
<dl><dt><img src="assets/icons/modes/inject.png" alt="Inject mode icon" /> Inject mode <kbd>I</kbd></dt><dd>In this mode, the user can move around the selected picture using <kbd>H</kbd><kbd>J</kbd><kbd>K</kbd><kbd>L</kbd> keys and draw with the currently selected tool. The color of the pixels being drawn is replaced by the selected color, so this can also work as an “eraser” if the selected color is transparent. If multiple pictures are opened, they can be switched between using <kbd>Ctrl</kbd>-<kbd>H</kbd> and <kbd>Ctrl</kbd>-<kbd>L</kbd>.</dd><dt><img src="assets/icons/modes/add.png" alt="Add mode icon" /> Add mode <kbd>A</kbd></dt><dd>In this mode, the user can move around the selected picture using <kbd>H</kbd><kbd>J</kbd><kbd>K</kbd><kbd>L</kbd> keys and draw with the currently selected tool. The selected color is added on the color of the pixels being drawn, which allows shading an area with a transparent color. If multiple pictures are opened, they can be switched between using <kbd>Ctrl</kbd>-<kbd>H</kbd> and <kbd>Ctrl</kbd>-<kbd>L</kbd>.</dd><dt><img src="assets/icons/modes/select.png" alt="Select mode icon" /> Select mode <kbd>V</kbd></dt><dd>In this mode, the user can move around the selected picture using <kbd>H</kbd><kbd>J</kbd><kbd>K</kbd><kbd>L</kbd> keys and select a part of the picture using the currently selected tool. The selection is currently useless; copy-paste functionality will be implemented later. If multiple pictures are opened, they can be switched between using <kbd>Ctrl</kbd>-<kbd>H</kbd> and <kbd>Ctrl</kbd>-<kbd>L</kbd>.</dd><dt><img src="assets/icons/modes/color.png" alt="Color mode icon" /> Color mode <kbd>C</kbd></dt><dd>In this mode, the user can move around a color palette (displayed at the top of the screen) to change the selected color (displayed in the upper left corner). If multiple palettes are available, they can be switched between using <kbd>Ctrl</kbd>-<kbd>H</kbd> and <kbd>Ctrl</kbd>-<kbd>L</kbd>.</dd><dt><img src="assets/icons/modes/command.png" alt="Command mode icon" /> Command mode <kbd>;</kbd></dt><dd>In this mode, the user can enter a command, displayed at the bottom of the screen, to perform complex actions such as opening a file. The commands are in the <a href="https://sprylang.se/">Spry</a> programming language, though this is not important for basic usage. The command is executed by pressing <kbd>⏎</kbd> or cancelled by pressing <kbd>Esc</kbd>.</dd></dl>
<p>The following tools are available:</p>
<dl><dt><img src="assets/icons/tools/brush.png" alt="Brush icon" /> Brush <kbd>B</kbd></dt><dd>The brush tool selects pixels that it moves over.</dd><dt><img src="assets/icons/tools/segment.png" alt="Segment icon" /> Segment <kbd>S</kbd></dt><dd>The segment tool selects a line segment.</dd><dt><img src="assets/icons/tools/rectangle-filled.png" alt="Rectangle icon" /> Rectangle <kbd>R</kbd></dt><dd>The rectangle tool selects an axis-aligned rectangle.</dd><dt><img src="assets/icons/tools/rectangle-outline.png" alt="Rectangle outline icon" /> Rectangle outline <kbd>Shift</kbd>-<kbd>R</kbd></dt><dd>The rectangle tool selects the outline of an axis-aligned rectangle.</dd><dt><img src="assets/icons/tools/ellipse-filled.png" alt="Ellipse icon" /> Ellipse <kbd>E</kbd></dt><dd>The ellipse tool selects an axis-aligned ellipse.</dd><dt><img src="assets/icons/tools/ellipse-outline.png" alt="Ellipse outline icon" /> Ellipse outline <kbd>Shift</kbd>-<kbd>E</kbd></dt><dd>The ellipse outline tool selects the outline of an axis-aligned ellipse.</dd><dt><img src="assets/icons/tools/flood.png" alt="Flood icon" /> Flood <kbd>F</kbd></dt><dd>The flood tool selects a contiguous area of pixels with the same color.</dd></dl></section>
<section><h2 class="xd-section-heading">Keyboard mappings</h2><p>The key mappings are inspired by Vim. They are currently not customizable.</p>
<table><tr><th>Key</th><th>Alone</th><th><kbd>Shift</kbd></th><th><kbd>Ctrl</kbd></th></tr>
<tr><td><kbd>⎵</kbd></td><td>hold to draw</td><td>hold to draw with secondary color</td></tr>
<tr><td><kbd>A</kbd></td><td>Mode ← Add</td></tr>
<tr><td><kbd>B</kbd></td><td>Tool ← Brush</td></tr>
<tr><td><kbd>C</kbd></td><td>Mode ← Color</td></tr>
<tr><td><kbd>D</kbd></td><td>delete pixel</td></tr>
<tr><td><kbd>E</kbd></td><td>Tool ← Ellipse</td><td>Tool ← Ellipse outline</td></tr>
<tr><td><kbd>F</kbd></td><td>Tool ← Flood</td></tr>
<tr><td><kbd>H</kbd></td><td>move left</td><td></td><td>previous picture/palette</td></tr>
<tr><td><kbd>I</kbd></td><td>Mode ← Inject</td></tr>
<tr><td><kbd>J</kbd></td><td>move down</td><td></td></tr>
<tr><td><kbd>K</kbd></td><td>move up</td><td></td></tr>
<tr><td><kbd>L</kbd></td><td>move right</td><td></td><td>next picture/palette</td></tr>
<tr><td><kbd>P</kbd></td><td>paste</td></tr>
<tr><td><kbd>Q</kbd></td><td>quit</td><td>force quit</td></tr>
<tr><td><kbd>R</kbd></td><td>Tool ← Rectangle</td><td>Tool ← Rectangle outline</td><td>redo</td></tr>
<tr><td><kbd>S</kbd></td><td>Tool ← Segment</td></tr>
<tr><td><kbd>U</kbd></td><td>undo</td></tr>
<tr><td><kbd>V</kbd></td><td>Mode ← Select</td></tr>
<tr><td><kbd>W</kbd></td><td>write picture</td><td>write all pictures</td></tr>
<tr><td><kbd>X</kbd></td><td>swap colors</td></tr>
<tr><td><kbd>Y</kbd></td><td>pick color</td><td>pick secondary color</td></tr>
<tr><td><kbd>-</kbd></td><td>zoom out</td><td>zoom out all pictures</td></tr>
<tr><td><kbd>=</kbd></td><td>zoom in</td><td>zoom in all pictures</td></tr>
<tr><td><kbd>;</kbd></td><td>Mode ← Command</td></tr></table>
Note that the mouse can also be used to draw and select colors. The right mouse button draws with the secondary color.
<section><h3 class="xd-section-heading">Selection keyboard mappings</h3><p>Some keys work differently when used with an active selection.</p>
<table><tr><th>Key</th><th>Alone</th><th><kbd>Shift</kbd></th><th><kbd>Ctrl</kbd></th></tr>
<tr><td><kbd>Esc</kbd></td><td>cancel selection</td></tr>
<tr><td><kbd>A</kbd></td><td>add selected color on selection</td></tr>
<tr><td><kbd>I</kbd></td><td>fill selection with selected color</td></tr>
<tr><td><kbd>Y</kbd></td><td>copy selection</td></tr></table></section></section>
<section><h2 class="xd-section-heading">Commands</h2><dl><dt><code>e image.png</code></dt><dd>open a file for editing (the filename is a symbol)</dd><dt><code>edit &quot;image.png&quot;</code></dt><dd>open a file for editing (the filename is a string)</dd><dt><code>flip-x</code></dt><dd>flip the current picture horizontally</dd><dt><code>flip-y</code></dt><dd>flip the current picture vertically</dd><dt><code>new</code></dt><dd>create a new picture with a transparent background and the default size</dd><dt><code>resize 16 8</code></dt><dd>resize the current picture, anchoring at the northwest corner</dd><dt><code>resize-nn 16 8</code></dt><dd>resize the current picture using the nearest-neighbor algorithm</dd><dt><code>w image.png</code></dt><dd>save the current picture under the specified filename (the filename is a symbol)</dd><dt><code>write &quot;image.png&quot;</code></dt><dd>save the current picture under the specified filename (the filename is a string)</dd></dl>
<p>Note that currently, edits done via commands are not added to undo history and break it.</p></section>
<section><h2 class="xd-section-heading">Planned features</h2><p>Roughly in order of priority.</p>
<ul><li>upscaling (various algorithms)</li><li>better editing support and tab completion for the command line</li><li>mirrored drawing</li><li>color picker, palette editing</li><li>variations of tools (wide brush, fill with tolerance)</li><li>programmable configuration</li><li>gradients</li><li>animated sprite editing</li></ul></section>
<section><h2 class="xd-section-heading">Non-features</h2><p>These things will never be added to Vier.</p>
<ul><li>GUI buttons</li><li>GUI animations</li><li>text tool</li><li>integrated web browser</li><li>advertisements</li><li>telemetry</li><li>cryptocurrency mining</li></ul></section>
<section><h2 class="xd-section-heading">Badges</h2><a href="https://notbyai.fyi/"><img src="./badges/not-by-ai.svg" alt="Written by human, not by AI" /></a>
<a href="https://nogithub.codeberg.page"><img src="./badges/no-github.svg" alt="Please don’t upload to GitHub" /></a></section>
<section><h2 class="xd-section-heading">Name</h2><p>I partially created Vier as a semestral project for a Computer Graphics course at my university. The name of the lecturer can be loosely translated as “fear”, so I decided to name the project after a German word which is pronounced the same way. The word means “four”, which hints at the editor treating images as a square grid (with squares having four sides, obviously). I also came up with the backronym “Vim-Inspired Editor of Rasters”.</p></section>
</body></html>