# © 2023 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

import ./palettes
import ./utils
import ./vector
import pkg/raylib
import pkg/spryvm/spryvm
import pkg/spryvm/sprycore
import std/bitops
import std/cmdline
import std/math except E
import std/os
import std/sequtils
import std/sets
import std/sugar
import std/unicode

type
  Mode = enum
    Inject
    Add
    Select
    Color
    Command

  ToolKind = enum
    Brush
    Flood
    Segment
    RectFilled
    RectOutline
    EllipseFilled
    EllipseOutline

  Tool = object
    case kind: ToolKind
    of Brush:
      brushSize: int32 # ignored for now
    of Flood:
      tolerance: int32
    else:
      discard

  PixelChange = object
    ## Represents that the color of a pixel was changed. Used for the undo functionality.
    pos: Vec
    originalColor: Color
    newColor: Color

  ImageChange = object
    ## Holds a list of all changes that were made in a single step. Used for the undo functionality.
    pixelChanges: seq[PixelChange]

  Picture = ref object
    image: Image
    texture: Texture
    fileName: string
    cursor: Vec
    anchor: Vec
    scale: int32
    selection: seq[Vec]
    history: seq[ImageChange]
    historyPos: int
    lastWritePos: int

  App = ref object
    spry: spryvm.Interpreter # Interpreter of the Spry language
    font: Font
    palettes: seq[Picture]
    pictures: seq[Picture]
    selectedPaletteIndex: int
    selectedPictureIndex: int
    color: Color
    secondaryColor: Color = color"#00000000"
    secondaryColorPos: tuple[palette: int, cursor: Vec] = (0, (1, 1))
    movementTime: int # Time before the cursor can be moved
    command: string
    mode: Mode
    tool: Tool
    clipboard: seq[PixelChange]

const
  backgroundColor0 = color"#555555"
  backgroundColor1 = color"#AAAAAA"
  codePoints = toSeq(32'i32 .. 126'i32) & @["…".runeAt(0).int32]
  defaultScale = 8'i32
  fps = 60'i32
  letterWidth = 8
  margin = 4'i32
  maxPaletteHeight = 156'i32
  movementTimeThreshold = fps div 3
  selectionInnerColor = color"#15AABF55"
  selectionOuterColor = color"#15AABFAA"
  statusSize = 64'i32 + margin
  textSize = 20
  textSpacing = 0
  unsavedNameColor = color"#FFD43B"

var modeIcons: array[Mode, Texture]
var toolIcons: array[ToolKind, Texture]
proc loadIcons() =
  modeIcons = [
    Inject: loadTextureStatic("icons/modes/inject.png"),
    Add: loadTextureStatic("icons/modes/add.png"),
    Select: loadTextureStatic("icons/modes/select.png"),
    Color: loadTextureStatic("icons/modes/color.png"),
    Command: loadTextureStatic("icons/modes/command.png")
  ]
  toolIcons = [
    Brush: loadTextureStatic("icons/tools/brush.png"),
    Flood: loadTextureStatic("icons/tools/flood.png"),
    Segment: loadTextureStatic("icons/tools/segment.png"),
    RectFilled: loadTextureStatic("icons/tools/rectangle-filled.png"),
    RectOutline: loadTextureStatic("icons/tools/rectangle-outline.png"),
    EllipseFilled: loadTextureStatic("icons/tools/ellipse-filled.png"),
    EllipseOutline: loadTextureStatic("icons/tools/ellipse-outline.png")
  ]

proc loadTexture(picture: Picture) =
  ## Creates a Texture for the picture so that it can be rendered on the screen.
  ## Should be called when creating or resizing a picture.
  picture.texture = loadTextureFromImage(picture.image)

proc updateTexture(picture: Picture) =
  ## Updates the picture’s texture to match the image data.
  ## Should be called after making a change to the image.
  picture.texture.updateTexture(picture.image.loadImageColors().toOpenArray())

proc newPicture(): Picture =
  ## Creates a blank picture with a hard-coded size.
  result = Picture(image: genImageColor(64, 64, Blank), scale: defaultScale)
  result.loadTexture()

proc newPicture(fileName: string): Picture =
  ## Creates a picture by opening the given file.
  result = Picture(
    image:
      try:
        loadImage(fileName)
      except RaylibError:
        genImageColor(64, 64, Blank)
    ,
    fileName: fileName,
    scale: defaultScale,
  )
  result.loadTexture()

proc canvasSize(picture: Picture): Vec =
  ## Returns the size in pixels that the picture’s canvas will be drawn on the screen.
  picture.scale * picture.image.size

proc widgetSize(picture: Picture, isPalette = false): Vec =
  ## Returns the size in pixels that the picture will be drawn on the screen.
  if isPalette:
    picture.canvasSize
  else:
    picture.canvasSize + (0'i32, margin + textSize)

proc `[]`(picture: Picture, pos: Vec): Color =
  picture.image.getImageColor(pos.x, pos.y)

proc colorAtCursor(picture: Picture): Color =
  picture[picture.cursor]

proc moveCursor(picture: Picture, movement: Vec) =
  ## Moves the cursor by the given amount, ensuring that it stays in bounds.
  picture.cursor.x = clamp(picture.cursor.x + movement.x, 0'i32 ..< picture.image.width)
  picture.cursor.y =
    clamp(picture.cursor.y + movement.y, 0'i32 ..< picture.image.height)

proc add(picture: Picture, change: ImageChange) =
  ## Adds the change to the picture’s change history. Does *not* apply the change.
  picture.history.setLen(picture.historyPos)
  picture.history.add(change)
  picture.historyPos.inc

proc apply(picture: Picture, change: PixelChange) =
  picture.image.imageDrawPixel(change.pos.x, change.pos.y, change.newColor)

proc apply(picture: Picture, change: ImageChange) =
  for pixelChange in change.pixelChanges:
    picture.apply(pixelChange)
  picture.updateTexture()

proc redo(picture: Picture) =
  if picture.historyPos < picture.history.len:
    picture.apply(picture.history[picture.historyPos])
    picture.historyPos.inc

proc undo(picture: Picture, change: PixelChange) =
  picture.image.imageDrawPixel(change.pos.x, change.pos.y, change.originalColor)

proc undo(picture: Picture, change: ImageChange) =
  for pixelChange in change.pixelChanges:
    picture.undo(pixelChange)
  picture.updateTexture()

proc undo(picture: Picture) =
  if picture.historyPos > 0:
    picture.historyPos.dec
    picture.undo(picture.history[picture.historyPos])

proc zoomIn(picture: Picture) =
  ## Increases the scale of the picture to approximately 5/4 the original,
  ## in a way that forms a nice sequence.
  picture.scale += 1'i32 shl max(0, picture.scale.fastLog2() - 2)

proc zoomOut(picture: Picture) =
  ## Inverse of the zoomIn operation.
  if picture.scale > 1:
    picture.scale -=
      1'i32 shl
      max(0, picture.scale.fastLog2() - 2 - int(picture.scale.countSetBits() == 1))

proc distance(x, y: Color): int32 =
  ## Calculates the taxicab distance between the two colors.
  abs(x.r.int32 - y.r.int32) + abs(x.g.int32 - y.g.int32) + abs(x.b.int32 - y.b.int32)

proc lineSegment(start, `end`: Vec, includeStart: bool): seq[Vec] =
  # Algorithm taken from https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
  if includeStart:
    result.add((start.x, start.y))
  let
    distanceX = abs(start.x - `end`.x)
    negDistanceY = -abs(start.y - `end`.y)
    signX = if start.x < `end`.x: 1'i32 else: -1'i32
    signY = if start.y < `end`.y: 1'i32 else: -1'i32
  var
    point = start
    error = distanceX + negDistanceY
  while point != `end`:
    let doubleError = 2 * error
    if doubleError >= negDistanceY:
      if point.x == `end`.x:
        result.add(point)
        break
      error += negDistanceY
      point.x += signX
    if doubleError <= distanceX:
      if point.y == `end`.y:
        result.add(point)
        break
      error += distanceX
      point.y += signY
    result.add(point)

proc rectangleFilled(a, b: Vec): seq[Vec] =
  collect:
    for y in min(a.y, b.y) .. max(a.y, b.y):
      for x in min(a.x, b.x) .. max(a.x, b.x):
        (x, y)

proc rectangleOutline(a, b: Vec): seq[Vec] =
  let u = min(a, b)
  let v = max(a, b)
  concat(
    collect(
      for x in u.x .. v.x:
        (x, u.y)
    ),
    collect(
      for x in u.x .. v.x:
        (x, v.y)
    ),
    collect(
      for y in u.y + 1 .. v.y - 1:
        (u.x, y)
    ),
    collect(
      for y in u.y + 1 .. v.y - 1:
        (v.x, y)
    ),
  )

proc ellipse(a, b: Vec, filled: bool): seq[Vec] =
  # Algorithm taken from http://members.chello.at/easyfilter/bresenham.pdf
  var
    a = a
    b = b
  if a.x > b.x:
    swap(a.x, b.x)
  if a.y > b.y:
    swap(a.y, b.y)
  let
    diameter = b - a
    parity = diameter.y and 1
    errorIncrementIncrement: Vec = (8 * diameter.y ^ 2, 8 * diameter.x ^ 2)
  var
    errorIncrement: Vec = (
      4 * (1 - diameter.x) * diameter.y ^ 2, 4 * (1 + parity) * diameter.x ^ 2
    )
    error = errorIncrement.x + errorIncrement.y + parity * diameter.x ^ 2
  a.y += (diameter.y + 1) div 2
  b.y = a.y - parity
  while a.x <= b.x:
    if filled:
      result.add(rectangleOutline(a, b))
    else:
      result.add([(a.x, a.y), (b.x, a.y), (a.x, b.y), (b.x, b.y)])
    let doubleError = 2 * error
    if doubleError <= errorIncrement.y:
      a.y.inc()
      b.y.dec()
      errorIncrement.y += errorIncrementIncrement.y
      error += errorIncrement.y
    if doubleError >= errorIncrement.x or 2 * error > errorIncrement.y:
      a.x.inc()
      b.x.dec()
      errorIncrement.x += errorIncrementIncrement.x
      error += errorIncrement.x
  while a.y - b.y <= diameter.y:
    result.add([(a.x - 1, a.y), (b.x + 1, a.y), (a.x - 1, b.y), (b.x + 1, b.y)])
    a.y.inc()
    b.y.dec()
  if filled:
    result = result.toHashSet.toSeq # deduplicate

proc select(picture: Picture, target: Vec, tool: Tool) =
  case tool.kind
  of Brush:
    # TODO: brush size
    if picture.selection.len == 0:
      picture.selection.add(target)
    else:
      picture.selection.add(
        lineSegment(picture.selection[^1], target, includeStart = false)
      )
  of Flood:
    var stack = @[target]
    var toSelect = initHashSet[Vec]()
    while stack.len != 0:
      let pos = stack.pop
      if pos in toSelect:
        continue
      let currentColor = picture[pos]
      toSelect.incl(pos)
      template check(x, y: int) =
        if distance(picture[(x, y)], currentColor) <= tool.tolerance:
          stack.add((x, y))

      if pos.x > 0:
        check(pos.x - 1, pos.y)
      if pos.x < picture.image.width - 1:
        check(pos.x + 1, pos.y)
      if pos.y > 0:
        check(pos.x, pos.y - 1)
      if pos.y < picture.image.height - 1:
        check(pos.x, pos.y + 1)
    picture.selection = toSelect.toSeq()
  of RectFilled:
    picture.selection = rectangleFilled(picture.anchor, target)
  of RectOutline:
    picture.selection = rectangleOutline(picture.anchor, target)
  of Segment:
    picture.selection = lineSegment(picture.anchor, target, includeStart = true)
  of EllipseFilled:
    picture.selection = ellipse(picture.anchor, target, filled = true)
  of EllipseOutline:
    picture.selection = ellipse(picture.anchor, target, filled = false)

proc injectColor(picture: Picture, color: Color) =
  ## Changes all selected pixels to the given color.
  let change = ImageChange(
    pixelChanges: collect(
      for pixel in picture.selection:
        PixelChange(pos: pixel, originalColor: picture[pixel], newColor: color)
    )
  )
  picture.add(change)
  picture.apply(change)

proc addColor(picture: Picture, color: Color) =
  ## Adds the given color to all selected pixels.
  let change = ImageChange(
    pixelChanges: collect(
      for pixel in picture.selection:
        let originalColor = picture[pixel]
        PixelChange(
          pos: pixel,
          originalColor: originalColor,
          newColor: colorAlphaBlend(originalColor, color, White),
        )
    )
  )
  picture.add(change)
  picture.apply(change)

proc clickPixel(picture: Picture, target: Vec, mode: Mode, tool: Tool, color: Color) =
  ## Reacts to clicking the image at the given point or pressing spacebar with the cursor on it.
  picture.selection.setLen(0)
  picture.anchor = target
  case mode
  of Inject:
    picture.select(target, tool)
    picture.injectColor(color)
  of Add:
    picture.select(target, tool)
    picture.addColor(color)
  of Select:
    picture.select(target, tool)
  of Color, Command:
    discard

proc dragPixel(picture: Picture, target: Vec, mode: Mode, tool: Tool, color: Color) =
  ## Reacts to dragging the mouse over the given point with the mouse button held down,
  ## or to moving the cursor over the point while holding spacebar.
  case mode
  of Inject:
    picture.select(target, tool)
    picture.undo()
    picture.injectColor(color)
  of Add:
    picture.select(target, tool)
    picture.undo()
    picture.addColor(color)
  of Select:
    picture.select(target, tool)
  of Color, Command:
    discard

proc isSaved(picture: Picture): bool =
  picture.historyPos == picture.lastWritePos

proc write(picture: Picture) =
  # TODO: else case
  if picture.fileName != "":
    # TODO: handle possible error
    discard picture.image.exportImage(picture.fileName.cstring)
    picture.lastWritePos = picture.historyPos

proc drawTextTruncated(
    font: Font, text: string, pos: Vec, maxWidth: int32, color: Color
) =
  const ellipsisColor = color"#666968"
  let maxLen = maxWidth div letterWidth
  if maxLen == 0:
    discard
  elif text.len <= maxLen:
    drawText(font, text.cstring, pos.toVector2, textSize, textSpacing, color)
  else:
    var pos = pos.toVector2
    let half = (maxLen - 2) div 2
    let firstPart = text[0 ..< half]
    drawText(font, firstPart.cstring, pos, textSize, textSpacing, color)
    pos.x += measureText(font, firstPart.cstring, textSize, textSpacing).x
    drawText(font, cstring"…", pos, textSize, textSpacing, ellipsisColor)
    pos.x += measureText(font, cstring"…", textSize, textSpacing).x
    let secondPart = text[^half ..^ 1]
    drawText(font, secondPart.cstring, pos, textSize, textSpacing, color)

proc drawBackground(origin: Vec, size: Vec) =
  drawRectangle(origin, size, backgroundColor0)
  for t in 0 ..< size.x + size.y:
    if t mod 8 >= 4:
      let xOverlap = max(0, t - size.x)
      let yOverlap = max(0, t - size.y)
      drawLine(
        origin + (t - xOverlap, xOverlap),
        origin + (yOverlap, t - yOverlap),
        backgroundColor1,
      )

proc draw(picture: Picture, app: App, origin: Vec, selected: bool) =
  ## Draws the picture on the screen with its northwest corner at the given point.
  let canvasSize = picture.canvasSize
  drawBackground(origin, canvasSize)
  drawTexture(
    picture.texture,
    Vector2(x: origin.x.float32, y: origin.y.float32),
    rotation = 0,
    scale = picture.scale.float32,
    tint = White,
  )
  drawRectangleLines(
    origin + picture.scale * picture.cursor, square(picture.scale), Black
  )
  drawRectangleLines(
    origin + picture.scale * picture.cursor + square(1),
    square(picture.scale - 2),
    White,
  )
  drawTextTruncated(
    app.font,
    picture.fileName.extractFilename(),
    (origin.x, origin.y + canvasSize.y + margin),
    canvasSize.x,
    if picture.isSaved: White else: unsavedNameColor,
  )
  if app.mode == Select:
    for pixel in picture.selection:
      drawRectangle(
        origin + picture.scale * pixel, square(picture.scale), selectionInnerColor
      )
      drawRectangleLines(
        origin + picture.scale * pixel, square(picture.scale), selectionOuterColor
      )
  if selected:
    drawRectangleLines(origin, canvasSize, White)
  else:
    drawRectangle(origin, canvasSize, color"#00000040")

iterator layout(
    pictures: openArray[Picture],
    pos: Vec,
    maxSize: Vec,
    selectedIndex: int32,
    isPalette = false,
): (int, Picture, Vec) =
  ## Lays out the given pictures into the given rectangle and yields each picture along with its position.
  ## If the pictures exceed the allowed width, prefers to place the selection at the center of the container.
  let totalSize: Vec = (
    x:
      pictures.mapIt(it.widgetSize(isPalette = isPalette).x).sum() +
      (pictures.len.int32 - 1) * margin,
    y: pictures.mapIt(it.widgetSize(isPalette = isPalette).y).max(),
  )
  let selectedPicture = pictures[selectedIndex]
  var origin =
    pos + (
      x: (
        if totalSize.x <= maxSize.x: 0'i32
        else:
          let center =
            pictures[0 ..< selectedIndex]
            .mapIt(it.widgetSize(isPalette = isPalette).x)
            .sum() + selectedIndex * margin +
            selectedPicture.scale * selectedPicture.cursor.x +
            selectedPicture.scale div 2
          let halfMax = maxSize.x div 2
          if center <= halfMax: 0
          elif totalSize.x - center <= halfMax:
            maxSize.x - totalSize.x
          else:
            halfMax - center
      ),
      y: (
        if totalSize.y <= maxSize.y: 0'i32
        else:
          let center =
            selectedPicture.scale * selectedPicture.cursor.y +
            selectedPicture.scale div 2
          let halfMax = maxSize.y div 2
          if center <= halfMax: 0
          elif totalSize.y - center <= halfMax:
            maxSize.y - totalSize.y
          else:
            halfMax - center
      ),
    )
  for index, picture in pictures:
    if origin.x + picture.widgetSize(isPalette = isPalette).x >= pos.x and
        origin.x <= pos.x + maxSize.x:
      yield (index, picture, origin)
    origin.x += picture.widgetSize(isPalette = isPalette).x + margin

proc selectedPicture(app: App): Picture =
  app.pictures[app.selectedPictureIndex]

proc selectedPalette(app: App): Picture =
  app.palettes[app.selectedPaletteIndex]

proc updateColor(app: App) =
  ## Sets the primary color to the color currently selected in the palette.
  let palette = app.selectedPalette
  app.color = palette.colorAtCursor

proc swapColors(app: App) =
  swap(app.color, app.secondaryColor)
  let cursor = app.secondaryColorPos.cursor
  app.secondaryColorPos.cursor = app.selectedPalette.cursor
  swap(app.selectedPaletteIndex, app.secondaryColorPos.palette)
  app.selectedPalette.cursor = cursor

proc clearSelection(picture: Picture) =
  picture.selection.setLen(0)

proc clearSelection(app: App) =
  app.selectedPicture.clearSelection()

proc activeSelection(app: App): bool =
  app.mode == Select and app.selectedPicture.selection.len != 0

proc copy(app: App) =
  let selection = app.selectedPicture.selection
  let xMin = selection.mapIt(it.x).min()
  let yMin = selection.mapIt(it.y).min()
  app.clipboard = collect:
    for pixel in selection:
      PixelChange(pos: pixel - (xMin, yMin), newColor: app.selectedPicture[pixel])

proc paste(app: App) =
  let picture = app.selectedPicture
  let cursor = picture.cursor
  let change = ImageChange(
    pixelChanges: collect(
      for pixel in app.clipboard:
        if cursor + pixel.pos < picture.image.size:
          PixelChange(
            pos: cursor + pixel.pos,
            originalColor: picture[pixel.pos],
            newColor: pixel.newColor,
          )
    )
  )
  picture.add(change)
  picture.apply(change)

proc ensureNonEmptyPictures(app: App) =
  ## Ensures that the app has at least one picture.
  ## Should be called whenever there is a possibility that the app has no pictures.
  if app.pictures.len == 0:
    app.pictures.add(newPicture())

proc drawStatus(app: App, origin: Vec) =
  const colorSize = 50
  drawBackground(origin + (statusSize - colorSize, 0'i32), square(colorSize))
  drawRectangle(
    origin + (statusSize - colorSize, 0'i32), square(colorSize), app.secondaryColor
  )
  drawBackground(origin + (0'i32, statusSize - colorSize), square(colorSize))
  drawRectangle(origin + (0'i32, statusSize - colorSize), square(colorSize), app.color)
  drawTexture(
    modeIcons[app.mode],
    (origin + (0'i32, statusSize + margin)).toVector2,
    rotation = 0,
    scale = 2,
    tint = White,
  )
  drawTexture(
    toolIcons[app.tool.kind],
    (origin + (margin + 32, statusSize + margin)).toVector2,
    rotation = 0,
    scale = 2,
    tint = White,
  )

proc draw(app: App) =
  let screenSize: Vec = (getScreenWidth(), getScreenHeight())
  drawRectangle(square(0), screenSize, Black)
  app.drawStatus((margin, margin))
  block palettes:
    let origin: Vec = (margin + statusSize + margin, margin)
    let maxSize: Vec = (screenSize.x - margin - origin.x, maxPaletteHeight)
    scissorMode(origin.x, origin.y, maxSize.x, maxSize.y):
      for index, palette, origin in layout(
        app.palettes, origin, maxSize, app.selectedPaletteIndex.int32, isPalette = true
      ):
        palette.draw(app, origin, selected = index == app.selectedPaletteIndex)
  block pictures:
    let origin: Vec = (margin, margin + maxPaletteHeight + margin)
    let maxSize: Vec = screenSize - (margin, margin + textSize + margin) - origin
    scissorMode(origin.x, origin.y, maxSize.x, maxSize.y):
      for index, picture, origin in layout(
        app.pictures, origin, maxSize, app.selectedPictureIndex.int32
      ):
        picture.draw(app, origin, selected = index == app.selectedPictureIndex)
  if app.command != "\xC0":
    drawText(
      app.font,
      app.command.cstring,
      (margin, screenSize.y - margin - textSize).toVector2,
      textSize,
      textSpacing,
      White,
    )

proc processKeyboard(app: App) =
  let shift = isKeyDown(LeftShift) or isKeyDown(RightShift)
  let control = isKeyDown(LeftControl) or isKeyDown(RightControl)
  if app.mode == Command:
    if isKeyPressed(Escape):
      app.mode = Inject
      app.command = ""
    elif isKeyPressed(Enter):
      app.mode = Inject
      try:
        let res = app.spry.eval("[" & app.command & "]")
        if res.isNil or res of NilVal:
          app.command = ""
        else:
          app.command = $res
      except:
        app.command = getCurrentExceptionMsg()
    elif isKeyPressed(Backspace):
      if app.command.len == 0:
        app.mode = Inject
      else:
        app.command.setLen(app.command.len - 1)
    else:
      while (var ch = getCharPressed(); ch != 0):
        if app.command == "\xC0": # evil hack to work around a bug
          app.command = ""
        else:
          app.command.add(Rune(ch))
  else:
    if isKeyPressed(Escape):
      if app.activeSelection:
        app.selectedPicture.selection.setLen(0)
    if isKeyPressed(A):
      if app.activeSelection:
        app.selectedPicture.addColor(app.color)
        app.clearSelection()
      else:
        app.mode = Add
    if isKeyPressed(B):
      app.tool = Tool(kind: Brush)
    if isKeyPressed(C):
      app.mode = Color
    if isKeyPressed(D):
      let picture = app.selectedPicture
      let change = ImageChange(
        pixelChanges:
          @[
            PixelChange(
              pos: picture.cursor, originalColor: picture.colorAtCursor, newColor: Blank
            )
          ]
      )
      picture.add(change)
      picture.apply(change)
    if isKeyPressed(E):
      if shift:
        app.tool = Tool(kind: EllipseOutline)
      else:
        app.tool = Tool(kind: EllipseFilled)
    if isKeyPressed(F):
      app.tool = Tool(kind: Flood)
    if isKeyPressed(I):
      if app.activeSelection:
        app.selectedPicture.injectColor(app.color)
        app.clearSelection()
      else:
        app.mode = Inject
    if isKeyPressed(P):
      app.paste()
    if isKeyPressed(Q):
      if shift or app.pictures.all(isSaved):
        closeWindow()
        quit(QuitSuccess)
      else:
        app.command = "There are unsaved pictures; use Shift+Q to force quit"
    if isKeyPressed(R):
      if control:
        app.selectedPicture.redo
      elif shift:
        app.tool = Tool(kind: RectOutline)
      else:
        app.tool = Tool(kind: RectFilled)
    if isKeyPressed(S):
      app.tool = Tool(kind: Segment)
    if isKeyPressed(U):
      app.selectedPicture.undo
    if isKeyPressed(V):
      app.mode = Select
    if isKeyPressed(W):
      if shift:
        app.pictures.apply(write)
      else:
        app.selectedPicture.write()
    if isKeyPressed(X):
      app.swapColors()
    if isKeyPressed(Y):
      if app.activeSelection:
        app.copy()
        app.clearSelection()
      else:
        app.color = app.selectedPicture.colorAtCursor
    if isKeyPressed(Semicolon):
      app.mode = Command
      app.command = "\xC0" # evil hack to work around a bug
    if isKeyPressed(Equal):
      if shift:
        app.pictures.apply(zoomIn)
      else:
        app.selectedPicture.zoomIn()
    if isKeyPressed(Minus):
      if shift:
        app.pictures.apply(zoomOut)
      else:
        app.selectedPicture.zoomOut()
    var movement =
      int32(isKeyDown(H)) * (-1'i32, 0'i32) + int32(isKeyDown(J)) * (0'i32, 1'i32) +
      int32(isKeyDown(K)) * (0'i32, -1'i32) + int32(isKeyDown(L)) * (1'i32, 0'i32)
    if movement == square(0):
      app.movementTime = 0
    else:
      if app.movementTime != 0 and app.movementTime < movementTimeThreshold:
        movement = square(0)
      app.movementTime.inc
    case app.mode
    of Inject, Add, Select:
      if movement != square(0):
        if control:
          app.selectedPictureIndex =
            clamp(app.selectedPictureIndex + movement.x, 0 .. app.pictures.high)
        else:
          app.selectedPicture.moveCursor(movement)
          if isKeyDown(Space) and not isKeyPressed(Space):
            app.selectedPicture.dragPixel(
              app.selectedPicture.cursor,
              app.mode,
              app.tool,
              if shift: app.secondaryColor else: app.color,
            )
      if isKeyPressed(Space):
        app.selectedPicture.clickPixel(
          app.selectedPicture.cursor,
          app.mode,
          app.tool,
          if shift: app.secondaryColor else: app.color,
        )
    of Color:
      if movement != square(0):
        if control:
          app.selectedPaletteIndex =
            clamp(app.selectedPaletteIndex + movement.x, 0 .. app.palettes.high)
          app.updateColor()
        else:
          app.selectedPalette.moveCursor(movement)
          app.updateColor()
    of Command:
      discard

proc processMouse(app: App) =
  let pos: Vec = (getMouseX(), getMouseY())
  let screenSize: Vec = (getScreenWidth(), getScreenHeight())
  var cursor = MouseCursor.default
  block palettes:
    let origin: Vec = (margin + statusSize + margin, margin)
    let maxSize: Vec = (screenSize.x - margin - origin.x, maxPaletteHeight)
    for index, palette, canvasOrigin in layout(
      app.palettes, origin, maxSize, app.selectedPaletteIndex.int32, isPalette = true
    ):
      if pos >= origin and pos >= canvasOrigin and pos < origin + maxSize and
          pos < canvasOrigin + palette.canvasSize:
        cursor = PointingHand
        if isMouseButtonPressed(Left):
          app.selectedPaletteIndex = index
          palette.cursor = (pos - canvasOrigin) div palette.scale
          app.updateColor()
        elif isMouseButtonPressed(Right):
          app.secondaryColorPos.palette = index
          app.secondaryColorPos.cursor = (pos - canvasOrigin) div palette.scale
          app.secondaryColor =
            palette[(app.secondaryColorPos.cursor.x, app.secondaryColorPos.cursor.y)]
        break
  block pictures:
    let origin: Vec = (margin, margin + maxPaletteHeight + margin)
    let maxSize: Vec = screenSize - (margin, margin + textSize + margin) - origin
    scissorMode(origin.x, origin.y, maxSize.x, maxSize.y):
      for index, picture, canvasOrigin in layout(
        app.pictures, origin, maxSize, app.selectedPictureIndex.int32
      ):
        if pos >= origin and pos >= canvasOrigin and pos < origin + maxSize and
            pos < canvasOrigin + picture.canvasSize:
          cursor = Crosshair
          if isMouseButtonPressed(Left):
            app.selectedPictureIndex = index
            picture.clickPixel(
              (pos - canvasOrigin) div picture.scale, app.mode, app.tool, app.color
            )
          elif isMouseButtonDown(Left):
            # TODO: only do this if the mouse has changed pixel position
            picture.dragPixel(
              (pos - canvasOrigin) div picture.scale, app.mode, app.tool, app.color
            )
          elif isMouseButtonPressed(Right):
            app.selectedPictureIndex = index
            picture.clickPixel(
              (pos - canvasOrigin) div picture.scale,
              app.mode,
              app.tool,
              app.secondaryColor,
            )
          elif isMouseButtonDown(Right):
            # TODO: only do this if the mouse has changed pixel position
            picture.dragPixel(
              (pos - canvasOrigin) div picture.scale,
              app.mode,
              app.tool,
              app.secondaryColor,
            )
          break
  setMouseCursor(cursor)

proc addApi(spry: var spryvm.Interpreter, app: App) =
  ## Adds functions for interacting with the editor to the Spry virtual machine.
  nimFunc("edit"):
    let fileNameNode = spry.evalArg()
    let fileName =
      if fileNameNode of StringVal:
        fileNameNode.StringVal.value
      else:
        raiseRuntimeException("File name not a string: " & $fileNameNode)
        ""
    app.pictures.add(newPicture(fileName))
    app.selectedPictureIndex = app.pictures.high
  nimFunc("e"):
    let fileNameNode = spry.arg()
    let fileName =
      if fileNameNode of Word:
        fileNameNode.Word.word
      elif fileNameNode of StringVal:
        fileNameNode.StringVal.value
      else:
        raiseRuntimeException("File name not a word or string: " & $fileNameNode)
        ""
    app.pictures.add(newPicture(fileName))
    app.selectedPictureIndex = app.pictures.high
  nimFunc("flip-x"):
    app.selectedPicture.image.imageFlipHorizontal()
    app.selectedPicture.loadTexture()
  nimFunc("flip-y"):
    app.selectedPicture.image.imageFlipVertical()
    app.selectedPicture.loadTexture()
  nimFunc("new"):
    app.pictures.add(newPicture())
    app.selectedPictureIndex = app.pictures.high
  nimFunc("resize"):
    let widthNode = spry.evalArg()
    let heightNode = spry.evalArg()
    if widthNode of IntVal and heightNode of IntVal:
      let width = widthNode.IntVal.value.int32
      let height = heightNode.IntVal.value.int32
      app.selectedPicture.image.imageResizeCanvas(width, height, 0, 0, Blank)
      app.selectedPicture.loadTexture()
    else:
      raiseRuntimeException("Dimensions must be integers")
  nimFunc("resize-nn"):
    let widthNode = spry.evalArg()
    let heightNode = spry.evalArg()
    if widthNode of IntVal and heightNode of IntVal:
      let width = widthNode.IntVal.value.int32
      let height = heightNode.IntVal.value.int32
      app.selectedPicture.image.imageResizeNN(width, height)
      app.selectedPicture.loadTexture()
    else:
      raiseRuntimeException("Dimensions must be integers")
  nimFunc("write"):
    let fileNameNode = spry.evalArg()
    let fileName =
      if fileNameNode of StringVal:
        fileNameNode.StringVal.value
      else:
        raiseRuntimeException("File name not a string: " & $fileNameNode)
        ""
    app.selectedPicture.fileName = fileName
    app.selectedPicture.write()
  nimFunc("w"):
    let fileNameNode = spry.arg()
    let fileName =
      if fileNameNode of Word:
        fileNameNode.Word.word
      elif fileNameNode of StringVal:
        fileNameNode.StringVal.value
      else:
        raiseRuntimeException("File name not a word or string: " & $fileNameNode)
        ""
    app.selectedPicture.fileName = fileName
    app.selectedPicture.write()

proc main() =
  let app = App(spry: spryvm.newInterpreter())
  app.spry.addCore()
  app.spry.addApi(app)
  when defined(release):
    # Hide terminal spam from Raylib
    setTraceLogLevel(Error)
  setConfigFlags(flags(VsyncHint, WindowMaximized, WindowResizable))
  setTargetFPS(fps)
  initWindow(800, 600, "vier")
  app.font = loadFontStatic("fonts/Iosevka-Regular.ttf", textSize, codepoints)
  loadIcons()
  app.pictures = commandLineParams().map(newPicture)
  app.ensureNonEmptyPictures()
  app.palettes =
    @[Picture(image: rgbBasic, scale: 12), Picture(image: colar, scale: 12)]
  app.palettes.apply(loadTexture)
  app.updateColor()
  while true:
    app.processKeyboard()
    app.processMouse()
    drawing:
      app.draw()

main()
