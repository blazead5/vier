# © 2023 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

import pkg/raylib

type Vec* = tuple[x: int32, y: int32]

func `+`*(u, v: Vec): Vec =
  (u.x + v.x, u.y + v.y)

func `-`*(u, v: Vec): Vec =
  (u.x - v.x, u.y - v.y)

func `*`*(n: int32, u: Vec): Vec =
  (n * u.x, n * u.y)

func `div`*(u: Vec, n: int32): Vec =
  (u.x div n, u.y div n)

func `+=`*(u: var Vec, v: Vec) =
  u.x += v.x
  u.y += v.y

func `-=`*(u: var Vec, v: Vec) =
  u.x -= v.x
  u.y -= v.y

func `*=`*(u: var Vec, n: int32) =
  u.x *= n
  u.y *= n

func `<`*(u, v: Vec): bool =
  u.x < v.x and u.y < v.y

func `<=`*(u, v: Vec): bool =
  u.x <= v.x and u.y <= v.y

func `>`*(u, v: Vec): bool =
  u.x > v.x and u.y > v.y

func `>=`*(u, v: Vec): bool =
  u.x >= v.x and u.y >= v.y

func abs*(u: Vec): Vec =
  (abs(u.x), abs(u.y))

func min*(u, v: Vec): Vec =
  (min(u.x, v.x), min(u.y, v.y))

func max*(u, v: Vec): Vec =
  (max(u.x, v.x), max(u.y, v.y))

func square*(n: int32): Vec =
  (n, n)

func toVector2*(u: Vec): Vector2 =
  Vector2(x: u.x.float32, y: u.y.float32)

proc drawRectangle*(position, size: Vec, color: Color) =
  drawRectangle(position.x, position.y, size.x, size.y, color)

proc drawRectangleLines*(position, size: Vec, color: Color) =
  drawRectangleLines(position.x, position.y, size.x, size.y, color)

proc drawLine*(startPos, endPos: Vec, color: Color) =
  drawLine(startPos.x, startPos.y, endPos.x, endPos.y, color)

proc size*(image: Image): Vec =
  (image.width, image.height)
