# © 2023 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

import pkg/raylib
import std/os
import std/strutils

proc color*(code: string): Color =
  if code.len == 7 and code[0] == '#':
    return Color(
      a: 255,
      r: code[1 .. 2].parseHexInt().uint8,
      g: code[3 .. 4].parseHexInt().uint8,
      b: code[5 .. 6].parseHexInt().uint8,
    )
  elif code.len == 9 and code[0] == '#':
    return Color(
      a: code[7 .. 8].parseHexInt().uint8,
      r: code[1 .. 2].parseHexInt().uint8,
      g: code[3 .. 4].parseHexInt().uint8,
      b: code[5 .. 6].parseHexInt().uint8,
    )
  else:
    raise newException(ValueError, "Invalid color: " & code)

const assetsDir = currentSourcePath().parentDir.parentDir / "assets"

proc loadTextureStatic*(name: static string): Texture =
  const dataStr = staticRead(assetsDir / name)
  let data = cast[seq[uint8]](dataStr)
  loadImageFromMemory(".png", data).loadTextureFromImage()

proc loadFontStatic*(
    name: static string, size: int32, codepoints: openArray[int32]
): Font =
  const dataStr = staticRead(assetsDir / name)
  let data = cast[seq[uint8]](dataStr)
  loadFontFromMemory(".ttf", data, size, codepoints)
