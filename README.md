<!--
  © 2023 Adam Blažek <blazead5@cvut.cz>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# vier

_Vier_ is a keyboard-focused pixel art editor heavily inspired by Vim.

**[Documentation](https://xigoi.srht.site/vier/)**
