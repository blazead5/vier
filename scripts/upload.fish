#!/usr/bin/env fish
# © 2023 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later
tar -cvz -C website . -f site.tar.gz
hut pages publish -d xigoi.srht.site -s vier site.tar.gz
rm site.tar.gz
