# © 2023 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

version = "2024.130.0"
author = "Adam Blažek"
description = "Vim-Inspired Editor of Rasters"
license = "GPL-3.0-or-later"
srcDir = "src"
bin = @["vier"]

requires "nim >= 2.0.0"

requires "naylib"
requires "spryvm"
